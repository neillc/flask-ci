FROM ubuntu:16.04

MAINTAINER Neill Cox "neill.cox@ingenious.com.au"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

COPY app /app
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]